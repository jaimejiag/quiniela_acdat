package com.example.jaime.quiniela;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {

    private static final String SERVIDOR = "http://alumno.club/superior/jaime/premios.php";
    AsyncHttpClient cliente;
    Button calcular;
    RadioButton xml, json;
    EditText apuesta, resultado,premios;
    public static final String TEMPORAL = "resultado.xml";
    Spinner formatos;


    ArrayList<String> apuestas = new ArrayList<>();
    ArrayList<Jornada> resultados ;
    ArrayList<ApuestaGanadora> premiadas = new ArrayList<>();



    int  aciertosJornada, aciertosTotales, jornadaFinal;

    static  public boolean creado = true;

    private AdapterView.OnItemSelectedListener spinerListener;
    String FICHERO = "";
    String formato = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        calcular = (Button) findViewById(R.id.btnCalcular);
        resultado = (EditText) findViewById(R.id.edtRes);
        apuesta = (EditText) findViewById(R.id.edtAp);
        premios = (EditText) findViewById(R.id.edtPre);
        xml = (RadioButton) findViewById(R.id.rbtXML);
        json = (RadioButton) findViewById(R.id.rbtJSON);
        cliente = new AsyncHttpClient();
        cliente.setConnectTimeout(3000);
        creado = false;
        formatos = (Spinner) findViewById(R.id.spnFormato);
        CargarFormatos();


        calcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (xml.isChecked())
                    descargaXML();

                if (json.isChecked())
                    descargaJSON();

                descargaApuestas();

                if(apuestas.size()==0 || resultados.size()==0)
                    Toast.makeText(MainActivity.this, "Error ....",Toast.LENGTH_LONG).show();
                else{
                    EscrutarQuiniela();

                    try {
                        GuardarPremios();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }


        });
        jornadaFinal = 0;
    }


    private void GuardarPremios() throws IOException, JSONException {
        FICHERO = premios.getText().toString().concat(formato);
        switch (formato){
            case ".txt":
                escribirInterna(FICHERO,premiadas,false,"UTF-8");
                break;
            case ".xml":
                try {
                    escribirXML(FICHERO);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case ".json":
                    escribirGson(FICHERO);
                break;
        }


    }


    private void descargaXML() {
        final ProgressDialog progreso = new ProgressDialog(this);
        File miFichero=new File(Environment.getExternalStorageDirectory().getAbsolutePath(), TEMPORAL);
        cliente.get(resultado.getText().toString(), new FileAsyncHttpResponseHandler(miFichero) {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }


            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {

                progreso.dismiss();
                Toast.makeText(MainActivity.this, throwable.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                progreso.dismiss();
                try {
                    Toast.makeText(MainActivity.this, "XML descargado correctamente", Toast.LENGTH_SHORT).show();
                    resultados = Analisis.analizarXML(file);

                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        });
    }


    private void descargaJSON() {
        final ProgressDialog progreso = new ProgressDialog(this);
        cliente.get(resultado.getText().toString(), new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                progreso.dismiss();
                Toast.makeText(MainActivity.this, throwable.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                progreso.dismiss();
                try {

                    Toast.makeText(MainActivity.this, "Json descargado correctamente", Toast.LENGTH_SHORT).show();
                    resultados = Analisis.analizarJSON(response);

                } catch (JSONException e) {
                    Log.e("ERROR", e.getLocalizedMessage());
                }
            }

        });

    }


    private void descargaApuestas() {
        String canal = String.valueOf(apuesta.getText().toString());
        File miArchivo = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "Apuestas.txt");

        final ProgressDialog progreso = new ProgressDialog(this);
        cliente.get(canal, new FileAsyncHttpResponseHandler(miArchivo) {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                Toast.makeText(MainActivity.this, "Fallo al descargar apuestas: " + throwable.getMessage(), Toast.LENGTH_LONG).show();

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                progreso.dismiss();
                Toast.makeText(getApplicationContext(), "Apuestas descargadas correctamente en: " + file.getPath(), Toast.LENGTH_LONG).show();
                apuestas = Analisis.leerFichero(file);

            }
        });


    }


    private void CargarFormatos(){
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.formatos, R.layout.support_simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        formatos.setAdapter(adapter);

        spinerListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, android.view.View view, int position, long id) {

                formato = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };

        formatos.setOnItemSelectedListener(spinerListener);
    }


    private void EscrutarQuiniela(){
        int partidos = 13;
        int aciertos = 0;
        ApuestaGanadora apuestaG;
        aciertosTotales = 0;
        String jornadaActual, apuestaActual = "";

            for (int i = 0; i < resultados.size(); i++) {
                jornadaActual = resultados.get(i).getResultado();
                aciertosJornada = 0;

                for (int j = 0; j < apuestas.size(); j++) {
                    apuestaG = new ApuestaGanadora();
                    aciertos = 0;
                    apuestaActual = apuestas.get(j);

                    for (int k = 0; k <= partidos; k++) {
                        if (jornadaActual.charAt(k) == apuestaActual.charAt(k))
                            aciertos++;
                    }

                    if (jornadaActual.charAt(13) == apuestaActual.charAt(13) &&
                            jornadaActual.charAt(14) == apuestaActual.charAt(14) && aciertos == 14)
                        aciertos++;

                    if (aciertos > 9) {
                        apuestaG.setAciertos(aciertos);
                        apuestaG.setJornada(resultados.get(i).getNumero());
                        aciertosJornada++;
                        switch (aciertos) {
                            case 10:
                                apuestaG.setPremio(resultados.get(i).getPremio10());
                                break;

                            case 11:
                                apuestaG.setPremio(resultados.get(i).getPremio11());
                                break;

                            case 12:
                                apuestaG.setPremio(resultados.get(i).getPremio12());
                                break;

                            case 13:
                                apuestaG.setPremio(resultados.get(i).getPremio13());
                                break;

                            case 14:
                                apuestaG.setPremio(resultados.get(i).getPremio14());
                                break;

                            case 15:
                                apuestaG.setPremio(resultados.get(i).getPleno());
                                break;
                        }

                        apuestaG.setApuesta(apuestaActual);
                        premiadas.add(apuestaG);
                    }
                }

                resultados.get(i).setAcertadas(aciertosJornada);
            }

            for (int i = 0; i < resultados.size(); i++) {
                aciertosTotales += resultados.get(i).getAcertadas();
            }
    }


    public void escribirInterna(String fichero, ArrayList<ApuestaGanadora> premios, Boolean anadir, String codigo) {
        File miFichero;
        miFichero = new File(getApplication().getFilesDir().toString(), fichero);
        escribir(miFichero,premios,anadir,codigo);
    }


    private void escribir(File fichero, ArrayList<ApuestaGanadora> premios, Boolean anadir, String codigo) {
        FileOutputStream fos = null;
        OutputStreamWriter osw = null;
        BufferedWriter out = null;
        String linea = "";

        try {

            fos = new FileOutputStream(fichero, anadir);

            osw = new OutputStreamWriter(fos, codigo);
            out = new BufferedWriter(osw);
            for (int i = 0; i < premios.size();i++){
                        linea = "Jornada " + premios.get(i).getJornada() + "\r\n" + "Apuesta: " + premios.get(i).getApuesta() + "/" + "Aciertos: " + premios.get(i).getAciertos() + "/" + "Premio: " + premios.get(i).getPremio() + "\r\n";
                        out.write(linea);

            }
        } catch (IOException e) {
            Log.e("Error de entrada/salida", e.getMessage());
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                Subir(fichero);
            } catch (IOException e) {
                Log.e("Error al cerrar", e.getMessage());
            }
        }

    }


    public void escribirXML(String fichero) throws FileNotFoundException, IOException {
        File miFichero;
        miFichero = new File(getApplication().getFilesDir().toString(), fichero);
        FileOutputStream fout = null;
        fout = new FileOutputStream(miFichero,false);
        XmlSerializer serializer = Xml.newSerializer();
        serializer.setOutput(fout, "UTF-8");
        serializer.startDocument(null, true);
        serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
        serializer.startTag(null, "ApuestasGanadoras");
        serializer.attribute(null, "total", String.valueOf(aciertosTotales));

        for (int j= 0; j < resultados.size(); j++){
                serializer.startTag(null, "jornada");
                serializer.attribute(null, "numero", String.valueOf(resultados.get(j).getNumero()));
                serializer.attribute(null, "resultado", String.valueOf(resultados.get(j).getResultado()));
                serializer.attribute(null, "acertadas", String.valueOf(resultados.get(j).getAcertadas()));

                for (int i = 0; i < premiadas.size(); i++) {
                    if (resultados.get(j).getNumero() == premiadas.get(i).getJornada()) {
                        serializer.startTag(null, "apuesta");
                        serializer.attribute(null, "aciertos", String.valueOf(premiadas.get(i).getAciertos()));
                        serializer.attribute(null, "premio", String.valueOf(premiadas.get(i).getPremio()));
                        serializer.text(premiadas.get(i).getApuesta());
                        serializer.endTag(null, "apuesta");
                }
            }
            serializer.endTag(null, "jornada");
        }
        serializer.endTag(null, "ApuestasGanadoras");
        serializer.endDocument();
        serializer.flush();
        fout.close();
        Subir(miFichero);

    }


    private void escribirGson(String file) throws IOException {
        File myFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), file);

        String texto = "";
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();

        Gson gson = gsonBuilder.create();

        texto = gson.toJson(premiadas);

        OutputStreamWriter out;
        out = new FileWriter(myFile);
        out.write(texto);
        out.close();
        Subir(myFile);

    }


    private void Subir(File fichero) {
        final ProgressDialog progreso = new ProgressDialog(MainActivity.this);

        Boolean existe =true;
        RequestParams params = new  RequestParams();

        try
        {
            params.put("param", fichero );
        } catch (FileNotFoundException e) {
            existe = false;
            Toast.makeText(MainActivity.this,"Error: "+ e.getMessage(),Toast.LENGTH_SHORT).show();

        }

        if(existe)
            RestClient.post(SERVIDOR, params, new TextHttpResponseHandler() {
                @Override
                public void onStart() {

                    progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progreso.setMessage("Conectando . . .");
                    ;
                    progreso.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            RestClient.cancelRequests(getApplicationContext(), true);

                        }

                    });
                    progreso.show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String response) {
                    progreso.dismiss();
                    Toast.makeText(MainActivity.this, "Subido con exito", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String response, Throwable t) {
                    progreso.dismiss();
                    Toast.makeText(MainActivity.this, t.getMessage().toString(), Toast.LENGTH_LONG).show();
                }
            });

    }
}
