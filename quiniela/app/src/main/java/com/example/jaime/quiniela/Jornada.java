package com.example.jaime.quiniela;

/**
 * Created by linux on 3/01/17.
 */
public class Jornada {
    private int numero;
    private Double premio10;
    private Double premio11;
    private Double premio12;
    private Double premio13;
    private Double premio14;
    private Double pleno;
    private String resultado;
    private int acertadas;

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public Double getPremio10() {
        return premio10;
    }

    public void setPremio10(Double premio10) {
        this.premio10 = premio10 / 100;
    }

    public Double getPremio11() {
        return premio11;
    }

    public void setPremio11(Double premio11) {
        this.premio11 = premio11/ 100;
    }

    public Double getPremio12() {
        return premio12;
    }

    public void setPremio12(Double premio12) {
        this.premio12 = premio12/ 100;
    }

    public Double getPremio13() {
        return premio13;
    }

    public void setPremio13(Double premio13) {
        this.premio13 = premio13/ 100;
    }

    public Double getPremio14() {
        return premio14;
    }

    public void setPremio14(Double premio14) {
        this.premio14 = premio14/ 100;
    }

    public Double getPleno() {
        return pleno;
    }

    public void setPleno(Double pleno) {
        this.pleno = pleno / 100;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public int getAcertadas() {
        return acertadas;
    }

    public void setAcertadas(int acertadas) {
        this.acertadas = acertadas;
    }
}
