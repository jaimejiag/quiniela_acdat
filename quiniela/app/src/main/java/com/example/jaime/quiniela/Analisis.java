package com.example.jaime.quiniela;

import android.util.Log;
import android.util.Xml;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by linux on 3/01/17.
 */
public class Analisis {

    public static ArrayList<String> leerFichero(File fichero){
        FileInputStream fis = null;
        InputStreamReader isr = null;
        BufferedReader in = null;
        String linea;
        ArrayList<String> apuestas = new ArrayList<>();

        try {
            fis = new FileInputStream(fichero);
            isr = new InputStreamReader(fis, "utf8");
            in = new BufferedReader(isr);

            while ((linea = in.readLine()) != null)
                if(linea.length() != 0)
                    apuestas.add(linea);
        } catch (IOException e) {
            Log.e("Error", e.getMessage());
        } finally {
            try {
                if (in != null) {
                    in.close();}
            } catch (IOException e) {
                Log.e("Error al cerrar", e.getMessage());
            }
        }
        return apuestas;
    }


    public static ArrayList<Jornada> analizarXML(File file) throws XmlPullParserException, IOException {
        boolean dentroItem = false;
        int eventType;
        ArrayList<Jornada> jornadas = new ArrayList<>();
        Jornada actual = null;
        XmlPullParser xpp = Xml.newPullParser();
        xpp.setInput(new FileReader(file));
        eventType=xpp.getEventType();
        String res = "";

        while (eventType!=XmlPullParser. END_DOCUMENT ){
            switch (eventType) {
                case XmlPullParser. START_TAG :
                    String etiqueta = xpp.getName();
                    if(etiqueta.equalsIgnoreCase("quiniela")){
                        actual = new Jornada();
                        res = "";
                        dentroItem = true;
                    }

                    if(etiqueta.equalsIgnoreCase("partit")){
                        res += xpp.getAttributeValue(null, "sig");
                    }

                    if(dentroItem && etiqueta.equals("quiniela")) {
                        actual.setNumero(Integer.parseInt(xpp.getAttributeValue(null, "jornada")));
                        actual.setPleno(Double.parseDouble(xpp.getAttributeValue(null, "el15")));
                        actual.setPremio14(Double.parseDouble(xpp.getAttributeValue(null, "el14")));
                        actual.setPremio13(Double.parseDouble(xpp.getAttributeValue(null, "el13")));
                        actual.setPremio12(Double.parseDouble(xpp.getAttributeValue(null, "el12")));
                        actual.setPremio11(Double.parseDouble(xpp.getAttributeValue(null, "el11")));
                        actual.setPremio10(Double.parseDouble(xpp.getAttributeValue(null, "el10")));

                    }
                    break;

                case XmlPullParser. END_TAG :
                    if(xpp.getName().equalsIgnoreCase("quiniela")) {
                        dentroItem = false;
                        actual.setResultado(res);
                        if (!(actual.getResultado().equals("")))
                            jornadas.add(actual);
                    }
                    break;
            }

            eventType = xpp.next();
        }

        return jornadas;
    }

    public static ArrayList<Jornada> analizarJSON(JSONObject respuesta) throws JSONException {
        JSONArray items, partidos;
        JSONObject item1, item2;
        Jornada actual;
        ArrayList<Jornada> jornadas = null;
        String res = "";
        items = respuesta.getJSONObject("quinielista").getJSONArray("quiniela");
        jornadas = new ArrayList<>();

        for (int i = 0; i < items.length(); i++) {
            actual = new Jornada();
            res = "";
            item1 = items.getJSONObject(i);
            actual.setNumero(item1.getInt("jornada"));
            actual.setPleno(item1.getDouble("el15"));
            actual.setPremio14(item1.getDouble("el14"));
            actual.setPremio13(item1.getDouble("el13"));
            actual.setPremio12(item1.getDouble("el12"));
            actual.setPremio11(item1.getDouble("el11"));
            actual.setPremio10(item1.getDouble("el10"));

            partidos = item1.getJSONArray("partit");

            for (int j = 0; j < partidos.length(); j++) {
                item2 = partidos.getJSONObject(j);
                res += item2.getString("sig");
            }

            actual.setResultado(res);

            jornadas.add(actual);
        }

        return jornadas;
    }
}
